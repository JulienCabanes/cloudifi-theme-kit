#!/usr/bin/env node --experimental-modules
import {
  compile,
  watch,
  link,
} from './index.js';

const [,, ...args] = process.argv;

if (args.indexOf('link') > -1 || args.indexOf('ln') > -1) {
  link();
} else if (args.indexOf('compile') > -1) {
  compile();
} else {
  // link();
  watch();
}
